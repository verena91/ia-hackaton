# coding=utf-8

import tweepy
from datetime import datetime
import Constants
import csv


# Fill the X's with the credentials obtained by
# following the above mentioned procedure.
consumer_key = Constants.consumer_key
consumer_secret = Constants.consumer_secret
access_key = Constants.access_key
access_secret = Constants.access_secret

# Function to extract tweets
def get_tweets(search):

		# Authorization to consumer key and consumer secret
		auth = tweepy.OAuthHandler(consumer_key, consumer_secret)

		# Access to user's access key and access secret
		auth.set_access_token(access_key, access_secret)

		# Calling api
		api = tweepy.API(auth)
		numberOfTweets = 1500

		with open('tweets.csv', 'w') as csvfile:
			filewriter = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
			filewriter.writerow(['Tweet', 'Created At'])
			cantidad = 0
			for t in tweepy.Cursor(api.search, q=search, tweet_mode="extended").items(numberOfTweets):
				cantidad = cantidad + 1
				filewriter.writerow([(t.full_text).encode("utf-8").strip(), t.created_at])
			print(cantidad)

# Driver code
if __name__ == '__main__':

	# Here goes the twitter handle for the user
	# whose tweets are to be extracted.
	get_tweets("@Ferreiromario1 bache -filter:retweets AND -filter:replies")
