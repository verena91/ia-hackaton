// Imports the Google Cloud client library.
const automl = require('@google-cloud/automl');
const fs = require('fs');
const nconf = require('nconf');

async function start() {

 nconf.argv()
   .env()
   .file({ file: 'config.json' });
 const projectId = nconf.get('projectId');
 const computeRegion = nconf.get('computeRegion'); //`region-name, e.g. "us-central1"`;
 const modelId = nconf.get('modelId');
 // const scoreThreshold = undefined; //`value between 0.0 and 1.0, e.g. "0.5"`;

  // console.log('projecId: ' + nconf.get('projectId'));
  // console.log('region: ' + nconf.get('computeRegion'));
  // console.log('modelId: ' + nconf.get('modelId'));
  // Create client for prediction service.
 const client = new automl.PredictionServiceClient();

 // Get the full path of the model.
 const modelFullId = client.modelPath(projectId, computeRegion, modelId);
 const params = {};

 // if (scoreThreshold) {
 //   params.score_threshold = scoreThreshold;
 // }

 // Set the payload by giving the content and type of the file.
 const payload = { textSnippet: {} };
 payload.textSnippet.content = "YOUR TEXT GOES HERE";
 payload.textSnippet.mimeType = "text/plain";

 // params is additional domain-specific parameters.
 // currently there is no additional parameters supported.
 const [response] = await client.predict({
   name: modelFullId,
   payload: payload,
   params: params,
 });
 console.log(`Prediction results:`);
 response.payload.forEach(result => {
   console.log(`Predicted class name: ${result.displayName}`);
   console.log(`Predicted class score: ${result.classification.score}`);
 });
}

start();
