# Predictor (Backend)
Proyecto para predecir tweets.
Pendiente: falta conectar con obtención de tweets cada x tiempo.

## Requisitos
Crear un archivo `config.json` que tendrá los parámetros del modelo entrenado a ser utilizado para realizar las predicciones.
```
"projectId": "xxx",
"computeRegion": "xxx",
"modelId": "xxx"
```

## Google Aplication Credentials
* Generar el archivo con las claves de Google Cloud Platform. Ir a https://console.cloud.google.com/iam-admin/serviceaccounts?_ga=2.242568838.-524415712.1565363638&project=iackaton&folder=&organizationId=&supportedpurview=project y de entre las acciones elegir "Generar Clave". Esto descargará un archivo .json. Ubicar el archivo dentro del directorio que se desee.

* Luego, dentro del servidor donde se ejecutará la predicción exportar las credenciales:
`export GOOGLE_APPLICATION_CREDENTIALS="[PATH]/[FILE].json"`
indicando el directorio donde se ubicó el archivo descargado de Google Cloud.

## Instalación
Una vez clonado el proyecto y tener instalado node, instalar las librerías:
`npm install`
Y ejecutar el project con:
`node predict.js` o `npm start`

## Referencias
En construcción...
