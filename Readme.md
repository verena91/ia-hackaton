# TweetScraper
Proyecto para obtener los tweets relacionados a quejas ciudadanas sobre baches en la ciudad de Asunción.

## Requisitos
Crear un archivo `Constants.py` que tendrá las credenciales a ser utilizadas.
```
consumer_key = "XXX"
consumer_secret = "XXX"
access_key = "XXX"
access_secret = "XXX"
```

## Instalación
Una vez clonado el proyecto y tener instalado Pip, instalar la librería utilizada para obtener los tweets.
`pip install tweetpy`
Y ejecutar el project con
`python run.py`

## Referencias
[Standard search operators](https://developer.twitter.com/en/docs/tweets/rules-and-filtering/overview/standard-operators)

